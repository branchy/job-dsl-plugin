package javaposse.jobdsl.dsl.helpers

import com.google.common.base.Preconditions
import javaposse.jobdsl.dsl.JobParent
import javaposse.jobdsl.dsl.JobType
import javaposse.jobdsl.dsl.WithXmlAction


class AWSHelper extends AbstractHelper {

    Map<String, Object> jobArguments

    AWSHelper(List<WithXmlAction> withXmlActions, JobType type) {
        super(withXmlActions, type)
    }

    /**
     * Activate GitHub push trigger for job
     */
    def gitHubPushTrigger() {
        execute {
            def triggerNode = it / 'triggers'
            def gitTriggerNode = triggerNode / 'com.cloudbees.jenkins.GitHubPushTrigger'
            gitTriggerNode.@plugin = "github@1.8"
            gitTriggerNode / 'spec'("")
        }
    }
    
    /**
     * Add Publish over SSH deploy step
     */
    def publishOverSSH (String serverName, String sourceFileMask, String remoteDirectory, String execCommand ) {
        execute {
            def publisherNode = it / 'publishers'
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'consolePrefix'("SSH: ")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'configName'(serverName)
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'verbose'("true")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'transfers' / 'jenkins.plugins.publish__over__ssh.BapSshTransfer' / 'remoteDirectory'(remoteDirectory)
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'transfers' / 'jenkins.plugins.publish__over__ssh.BapSshTransfer' / 'sourceFiles'(sourceFileMask)
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'transfers' / 'jenkins.plugins.publish__over__ssh.BapSshTransfer' / 'excludes'("")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'transfers' / 'jenkins.plugins.publish__over__ssh.BapSshTransfer' / 'removePrefix'("")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'transfers' / 'jenkins.plugins.publish__over__ssh.BapSshTransfer' / 'remoteDirectorySDF'("false")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'transfers' / 'jenkins.plugins.publish__over__ssh.BapSshTransfer' / 'flatten'("false")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'transfers' / 'jenkins.plugins.publish__over__ssh.BapSshTransfer' / 'cleanRemote'("false")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'transfers' / 'jenkins.plugins.publish__over__ssh.BapSshTransfer' / 'noDefaultExcludes'("false")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'transfers' / 'jenkins.plugins.publish__over__ssh.BapSshTransfer' / 'makeEmptyDirs'("false")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'transfers' / 'jenkins.plugins.publish__over__ssh.BapSshTransfer' / 'patternSeparator'("[, ]+")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'transfers' / 'jenkins.plugins.publish__over__ssh.BapSshTransfer' / 'execCommand'(execCommand)
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'transfers' / 'jenkins.plugins.publish__over__ssh.BapSshTransfer' / 'execTimeout'("120000")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'transfers' / 'jenkins.plugins.publish__over__ssh.BapSshTransfer' / 'usePty'("false")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'useWorkspaceInPromotion'("false")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'publishers' / 'jenkins.plugins.publish__over__ssh.BapSshPublisher' / 'usePromotionTimestamp'("false")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'continueOnError'("false")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'failOnError'("false")
            publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'alwaysPublishFromMaster'("false")
            
            def hostConfigurationAccessNode = publisherNode / 'jenkins.plugins.publish__over__ssh.BapSshPublisherPlugin' / 'delegate' / 'hostConfigurationAccess'("")
            hostConfigurationAccessNode.@class = "jenkins.plugins.publish_over_ssh.BapSshPublisherPlugin"
            hostConfigurationAccessNode.@reference = "../.."

        }
    }

}